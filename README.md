# README #
Using the script in this repo, the results of Stijn De Beugher will be reproduced in no time! 👌

### Quick start 🚀 ###
To get a description of the usage of the script, just run (only on Linux machines... If your lucky any Unix machine will work, but there's no guarantee at all)

```
git clone https://florisdf@bitbucket.org/florisdf/reproduce_sdb_results.git
cd reproduce_sdb_results
bash steps_to_reproduce.sh -h
```

### 🚑 In case of emergency 🚑 ###
floris.defeyter@kuleuven.be