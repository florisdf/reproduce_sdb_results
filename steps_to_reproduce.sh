#!/usr/bin/env bash

# Parse the arguments
OPTIND=1 # Reset in case getopts has been used previously in the shell

usage="Perform all the steps from dataset to facevector generation in order to be able to reproduce the results from Stijn De Beugher.

Usage:
$(basename "$0") [-h] -p PARENT -d DATA -l DLIB

Options:
	-h (optional) display this help message
	-p (required) the directory in which subfolders will be created with the data and code (e.g. /home/bob/kifare_test)
	-d (required) the directory in which the Annotations-closeUP, Annotations-NoCloseUP and raw_filtered folders are stored (e.g. /home/bob/data/Klas_jade)
	-l (required) the directory in which the dlib repository was cloned and compiled (e.g. /home/bob/my_clones/dlib)
"

p_flag=false
d_flag=false
l_flag=false

while getopts ":hp:d:l:" opt; do
	case  $opt in
		h) echo "$usage"
			exit
			;;
		p) PARDIR=$OPTARG
			p_flag=true
			;;
		d) ORIGINAL_DATA_DIR=$OPTARG
			d_flag=true
			;;
		l) DLIB_LIB_DIR=$OPTARG
			l_flag=true
			;;
		:) printf "Missing argument for -%s\n" "$OPTARG" >&2
			echo "$usage" >&2
			exit 1
			;;
		\?) printf "Illegal option: -%s\n" "$OPTARG" >&2
			echo "$usage" >&2
			exit 1
			;;
	esac
done
shift $((OPTIND - 1))

if ! $d_flag || ! $p_flag || ! $l_flag; then
	printf "\nYou need to provide all the required arguments... (see below)\n\n" >&2
	echo "$usage" >&2
	exit 1
fi

# Check if all the dependencies are installed
declare -a dependencies=('qmake' 'cmake' 'git')
for dep in "${arr[@]}"
do
	command -v $dep >/dev/null 2>&1 || { echo >&2 "$(basename "$0") requires $dep, but it's not installed. Aborting."; exit 1; }
done

# Check if the DLIB folder has the correct sub-folders
if [ ! -f "$DLIB_LIB_DIR/dlib/cmake" ]; then
	printf "Wrong dlib directory $DLIB_LIB_DIR\n"
	printf "The Dlib directory must contain a subdirectory 'dlib' with a file 'cmake'\n"
	exit 1
fi

# Check if the data directory has the correct subdirectories
if [ ! -d "$ORIGINAL_DATA_DIR/Annotations-closeUP" ] || [ ! "$ORIGINAL_DATA_DIR/Annotations-NoCloseUP" ] || [ ! "$ORIGINAL_DATA_DIR/raw_filtered" ]; then
	printf "Wrong data directory $ORIGINAL_DATA_DIR\n"
	printf "The data directory must contain subdirectories Annotations-closeUP, Annotations-NoCloseUP and raw_filtered\n"
	exit 1
fi

# Check if the parent directory is empty and prompt if so
if [ -d "$PARDIR" ] && [ "$(ls -A $PARDIR)" ]; then
	while true; do
		echo
		read -p "The given parent directory ($PARDIR) is not empty. Is it okay to overwrite its files? (y/n) " yn
		case $yn in
			[Yy]* ) printf "Continuing...\n"; break;;
			[Nn]* ) printf "Aborting...\n"; exit 1;;
			* ) echo "Please answer yes or no.";;
		esac
	done
fi

# Create parent directory
if [ ! -d "$PARDIR" ]; then
	mkdir "$PARDIR"
fi

DATA_DIR="$PARDIR/data"
GIT_DIR="$PARDIR/KIFARE"

ANNOT_CLOSE="$DATA_DIR/Annotations-closeUP"
ANNOT_NOCLOSE="$DATA_DIR/Annotations-NoCloseUP"
REFS="$DATA_DIR/raw_filtered"

# Clone the KIFARE git files
if [ ! -d "$GIT_DIR" ]; then
	git clone https://gitlab.com/EAVISE/KIFARE.git "$GIT_DIR"
fi

# Copy the dataset (run in parallel)
if [ ! -d "$DATA_DIR" ]; then
	mkdir "$DATA_DIR"
	cp -a "$ORIGINAL_DATA_DIR/Annotations-closeUP/." "$ANNOT_CLOSE" &
	cp -a "$ORIGINAL_DATA_DIR/Annotations-NoCloseUP/." "$ANNOT_NOCLOSE" &
	cp -a "$ORIGINAL_DATA_DIR/raw_filtered/." "$REFS" &
	wait
fi

# Clean old files
find "$DATA_DIR" -name "Detections_*.xml" | while read f; do rm "$f"; done
find "$DATA_DIR" -name "*_files.txt" | while read f; do rm "$f"; done

# Put the correct paths in the xml files
find "$ANNOT_NOCLOSE" -name '*.xml' | while read f; do sed -i -e 's@\(<path>\).*\(/[^/]*\.jpg</path>\)@\1'$ANNOT_NOCLOSE'\2@' "$f"; done
find "$ANNOT_CLOSE" -name '*.xml' | while read f; do sed -i -e 's@\(<path>\).*\(/[^/]*\.jpg</path>\)@\1'$ANNOT_CLOSE'\2@' "$f"; done

# Create XML_files.txt files
find "$ANNOT_NOCLOSE" -name '*.xml' | sed -e 's@'$ANNOT_NOCLOSE'@.@' > "$ANNOT_NOCLOSE/XML_files.txt"
find "$ANNOT_CLOSE" -name '*.xml' | sed -e 's@'$ANNOT_CLOSE'@.@' > "$ANNOT_CLOSE/XML_files.txt"

# Create Image_files.txt files
find "$ANNOT_NOCLOSE" -name '*.jpg' | sed -e 's@'$ANNOT_NOCLOSE'@.@' > "$ANNOT_NOCLOSE/Image_files.txt"
find "$ANNOT_CLOSE" -name '*.jpg' | sed -e 's@'$ANNOT_CLOSE'@.@' > "$ANNOT_CLOSE/Image_files.txt"
find "$REFS" -mindepth 1 -type d | while read d; do find "$d" -name '*.jpg' | sed -e 's@'$d'@.@' > "$d/Image_files.txt"; done


# Compile DLIB face detection code (if needed)
DLIB_DETECT_DIR="$GIT_DIR/FaceDetection/detect_DLIB"
DLIB_DETECT_BUILD_DIR="$DLIB_DETECT_DIR/build"
DLIB_DETECT_BIN="$DLIB_DETECT_BUILD_DIR/detect_DLIB"
if [ ! -f "$DLIB_DETECT_BIN" ]; then
	sed -i -e "s@\(include(\).*\(/dlib/cmake)\)@\1"$DLIB_LIB_DIR"\2@" "$DLIB_DETECT_DIR/CMakeLists.txt"	
	if [ ! -d "$DLIB_DETECT_BUILD_DIR" ]; then
		mkdir "$DLIB_DETECT_BUILD_DIR"
	fi
	cd "$DLIB_DETECT_BUILD_DIR"
	cmake ..
	make -j$(nproc)
fi

# Create DLIB detection xml's (run it in parallel)
cd "$DLIB_DETECT_BUILD_DIR"
"$DLIB_DETECT_BIN" "$ANNOT_CLOSE/" &
"$DLIB_DETECT_BIN" "$ANNOT_NOCLOSE/" &
find "$REFS" -mindepth 1 -type d | while read d; do "$DLIB_DETECT_BIN" "$d/"; done &
wait

# Compile code to add ID's to detections (if needed)
MERGE_ANNODET_BUILD_DIR="$GIT_DIR/MergeAnnoDet"
MERGE_ANNODET_BIN="$MERGE_ANNODET_BUILD_DIR/MergeAnnoDet"
if [ ! -f "$MERGE_ANNODET_BIN" ]; then
	cd "$MERGE_ANNODET_BUILD_DIR"
	qmake -makefile
	make -j$(nproc)
fi

# Add ID's to detection xml's
ID_MERGE_CONFIG_FILE="$MERGE_ANNODET_BUILD_DIR/.temp.ini"

declare -a arr=("$ANNOT_CLOSE" "$ANNOT_NOCLOSE")
for d in "${arr[@]}"
do
	PATHDET="$d/Detections_DLIB.xml"
	PATHANNOT="$d/"
	ID_MERGE_CONFIG="PathDetections=$PATHDET
	PathAnnotations=$PATHANNOT
	overlapTH=0.5"
	echo "$ID_MERGE_CONFIG" > "$ID_MERGE_CONFIG_FILE"
	"$MERGE_ANNODET_BIN" "$ID_MERGE_CONFIG_FILE"
	mv ./temp.xml "$d/Detections_DLIB_ID.xml"
done

# Compile dlib face recognition code (if needed)
DLIB_RECOG_DIR="$GIT_DIR/DLIB_FaceRecog"
DLIB_RECOG_BUILD_DIR="$DLIB_RECOG_DIR/build"
DLIB_RECOG_BIN="$DLIB_RECOG_BUILD_DIR/DLIB_FaceRecog"
if [ ! -f "$DLIB_RECOG_BIN" ]; then
	sed -i -e "s@\(include(\).*\(/dlib/cmake)\)@\1"$DLIB_LIB_DIR"\2@" "$DLIB_RECOG_DIR/CMakeLists.txt"
	if [ ! -d "$DLIB_RECOG_BUILD_DIR" ]; then
		mkdir "$DLIB_RECOG_BUILD_DIR"
	fi
	cd $DLIB_RECOG_BUILD_DIR
	cmake ..
	make -j$(nproc)
fi

# Create FaceVector xml's (run it in parallel)
cd "$DLIB_RECOG_BUILD_DIR"
"$DLIB_RECOG_BIN" "$ANNOT_CLOSE/Detections_DLIB_ID.xml" "$ANNOT_CLOSE/" &
"$DLIB_RECOG_BIN" "$ANNOT_NOCLOSE/Detections_DLIB_ID.xml" "$ANNOT_NOCLOSE/" &
find "$REFS" -mindepth 1 -type d | while read d; do "$DLIB_RECOG_BIN" "$d/Detections_DLIB.xml" "$d/"; done &
wait

MAT_INST_FILE="$PARDIR/matlab_instructions.txt"
printf "\n\n\n**************************************************************\n"
printf "\nEverything is done... Now it's your turn!\n"
printf "\nPlease open the file $MAT_INST_FILE and follow the instructions\n"
printf "\n**************************************************************\n"

matlab_instructions="**************************************************
##################################################
**************************************************

- Open MATLAB

- Set $GIT_DIR/FaceClustering1 as working directory
 
**************************************************
##################################################
**************************************************

- Open convert_dets_to_mat.m

- Set the 'location' variable to 'dets_mats'

- Set the 'filename' variable to ['$ANNOT_CLOSE/Detections_FaceVector.xml']
- Set the 'name_v' variable to [location '/detections_closeup']
- Set 'name_nv' variables to [location '/detections_closeup_nv']
- Run the convert_dets_to_mat.m script

- Set the 'filename' variable to ['$ANNOT_NOCLOSE/Detections_FaceVector.xml']
- Set the 'name_v' variable to [location '/detections_no_closeup']
- Set 'name_nv' variables to [location '/detections_no_closeup_nv']
- Run the convert_dets_to_mat.m script again

**************************************************
##################################################
**************************************************

- Open the convert_ref_to_mat.m script

- Set the 'names' variable to
{'A1', 'A2', 'B1', 'B2','B3', 'C1', 'C2', 'E1', 'F1', 'F2', 'I1', 'I2', 'J1' ,'K1', 'L1', 'L2', 'L3', 'L4', 'M1', 'M2', 'M3', 'M4', 'N1', 'N2', 'S1', 'S2', 'S3', 'T1'}

- Set the 'path' variable to '$REFS/'
- Set the 'location' variable to 'ref_mats_filtered'
- Run the convert_ref_to_mat.m script

**************************************************
##################################################
**************************************************

- Open the FaceRecogV1.m script
- Set the 'names' variable to
{'A1', 'A2', 'B1', 'B2','B3', 'C1', 'C2', 'E1', 'F1', 'F2', 'I1', 'I2', 'J1' ,'K1', 'L1', 'L2', 'L3', 'L4', 'M1', 'M2', 'M3', 'M4', 'N1', 'N2', 'S1', 'S2', 'S3', 'T1'}

- Set the 'det_data' variable to load('dets_mats/detections_closeup.mat')
- Set the 'filename' variable to strcat('ref_mats_filtered/', names(kids), '.mat')
- Create a directory called 'PRdata' in the working directory
- Run the FaceRecogV1.m script
- Open PRCurve.m and run it
- Now you will see the PR-curve for the close-up images plotted in blue (Figure 2)

- Open the FaceRecogV1.m script
- Set the 'det_data' variable to load('dets_mats/detections_no_closeup.mat')
- Run the FaceRecogV1.m script
- Open PRCurve.m and run it
- The PR-curve for the non-close-up images will be plotted in orange on top of the previous PR-curve (Figure 2)

**************************************************
##################################################
**************************************************
"

printf "$matlab_instructions" > $MAT_INST_FILE
